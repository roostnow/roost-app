module.exports = {
  apps: [{
    name: "Roost App",
    script: "./app.js",
    watch: false,
    env: {
      "NODE_ENV": "production",
      "PORT": 3000
    }
  }]
}
