module.exports.uploads = {
  adapter: require('skipper-disk'),
  maxBytes: 100 * 1000 * 1000000, // 1 GB
  dirpath: '.tmp/uploads'
};
