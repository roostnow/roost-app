var monthNames = [
  "January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

var rent;
var started;

var loadData = function (cb) {
  $.get('/api/v1/calculator/fetch-data', function (data) {
    rent = Number(data.rent);
    started = new Date(data.started);
    return cb({
      rent: Number(data.rent),
      started: new Date(data.started)
    });
  });
};

var saveData = function (data) {
  $.post('/api/v1/calculator/submit-calculator', data);
};

var monthDiff = function (dateFrom, dateTo) {
  return dateTo.getMonth() - dateFrom.getMonth() +
    (12 * (dateTo.getFullYear() - dateFrom.getFullYear()));
};

var renderKitty = function (rent, started) {
  var now = new Date();
  var months = monthDiff(started, now);

  var kitty = rent * months * 0.08;

  $('.js-kitty').text(kitty);
  $('.js-monthly-payment').text(rent);
  $('.js-since-date').text(
    monthNames[started.getMonth()] + ' ' + started.getFullYear()
  );
  if ($('#rent').val() === '') {
    $('#rent').val(rent);
  }
  if ($('#months').val() === '') {
    $('#months').val(months);
  }
};

loadData(function (data) {
  if (rent && started) {
    renderKitty(rent, started);
    $('.js-rent-calculator').css(
      'transition', 'none'
    ).addClass(
      'rent-calculator--active rent-calculator--complete'
    ).css(
      'transition', null
    );
  }
});

$('.js-rent-calculator').on('click', function () {
  if (!$(this).is('.rent-calculator--active')) {
    $(this).addClass('rent-calculator--active');
  }
});

$('.js-rent-calculator-form').on('submit', function (e) {
  e.preventDefault();

  var rent = parseInt($('#rent').val());
  var months = parseInt($('#months').val());
  var started = new Date();
  started.setMonth(started.getMonth() - months);

  saveData({
    'monthlyRent': rent,
    'started': started
  });

  renderKitty(rent, started);

  $('.js-rent-calculator').addClass('rent-calculator--complete');
});

$('.js-edit-calculation').on('click', function () {
  $('.js-rent-calculator').removeClass('rent-calculator--complete');
});
