jQuery(function($){
    $('.js-roost-alert').each(function(){
        var $el = $(this);
        var $steps = $(this).children('.roost-alert__step');

        var getCurrentStep = function() {
            return $el.data('step') || 0;
        }

        var setCurrentStep = function(i) {
            $el.data('step', i);
        }

        function updateUI() {
            var currentStep = getCurrentStep();
            if ( currentStep > 0 ) {
                $el.addClass('roost-alert--active');
                $steps.each(function(i){
                    $(this).toggleClass('roost-alert__step--active', i === currentStep);
                });
            } else {
                $el.removeClass('roost-alert--active');
            }
        }

        updateUI();

        $el.on('click', '.js-roost-alert-next-step', function(){
            setCurrentStep( getCurrentStep() + 1 );
            updateUI();
        });

    });
});
