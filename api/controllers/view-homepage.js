module.exports = {

  friendlyName: 'View homepage',

  description: 'Display homepage.',

  inputs: {},


  exits: {
    success: {
      viewTemplatePath: 'pages/homepage'
    },
  },

  fn: async function (inputs, exits) {

    var images = await Image.find({
      owner: this.req.session.userId
    });

    console.log('userId', this.req.session.userId, 'viewed their homepage');

    return exits.success({
      images
    });
  }

};
