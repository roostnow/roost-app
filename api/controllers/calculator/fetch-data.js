module.exports = {


  friendlyName: 'Fetch data',


  description: 'Get the data for the calculator',


  inputs: {

  },


  exits: {

  },


  fn: async function (inputs, exits) {
    var user = await User.findOne(this.req.session.userId);

    if (!user) {
      return exits.success({});
    }

    return exits.success({
      rent: user.monthlyRent,
      started: user.started
    });

  }


};
