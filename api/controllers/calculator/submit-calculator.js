module.exports = {


  friendlyName: 'Submit calculator',


  description: 'Submit the homepage calculator',


  inputs: {
    monthlyRent: {
      type: 'number',
      min: 0,
      required: true
    },
    started: {
      type: 'string',
      description: 'The number of months that the user has been renting.'
    }
  },


  exits: {},


  fn: async function (inputs, exits) {

    await User.updateOne(this.req.session.userId).set({
      monthlyRent: inputs.monthlyRent,
      started: inputs.started
    });

    return exits.success();
  }
};
