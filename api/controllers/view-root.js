module.exports = {

  friendlyName: 'Root',

  description: 'Nice blank page.',

  exits: {
    success: {
      viewTemplatePath: 'pages/root'
    }
  },

  fn: async function (inputs, exits) {
    return exits.success();
  }

};
