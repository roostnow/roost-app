module.exports = {
  friendlyName: 'Signup',
  description: 'Signup user.',
  inputs: {
    firstName: {
      type: 'string',
      required: true,
      minLength: 2,
      maxLength: 20
    },
    lastName: {
      type: 'string',
      required: true,
      minLength: 2,
      maxLength: 20
    },
    email: {
      type: 'string',
      isEmail: true,
      required: true
    },
    addressLineOne: {
      type: 'string',
      minLength: 5,
      maxLength: 40,
      required: true
    },
    postcode: {
      type: 'string',
      required: true
    }
  },


  exits: {},

  fn: async function (inputs, exits) {
    inputs.walletId = this.req.session.walletId;
    var user = await User.create(inputs).fetch();

    this.req.session.userId = user.id;

    return this.res.redirect('/home');
  }


};
