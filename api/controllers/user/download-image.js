module.exports = {


  friendlyName: 'Download image',


  description: 'Download the image from the id specified.',

  inputs: {
    imageId: {
      type: 'number',
      required: true
    }
  },

  exits: {
    success: {
      outputDescription: 'The streaming bytes of the specified thing\'s photo.',
      outputType: 'ref'
    },
    notFound: {
      responseType: 'notFound'
    },
    unauthorised: {
      responseType: 'unauthorised'
    }
  },


  fn: async function (inputs, exits) {

    var image = await Image.findOne(inputs.imageId);
    if (!image) {
      throw 'notFound'
    }

    if (image.owner != this.req.session.userId) {
      throw 'unauthorised';
    }

    this.res.type(image.imageMime);

    var downloading = await sails.startDownload(image.imageFd);
    return exits.success(downloading);

  }


};
