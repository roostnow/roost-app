module.exports = {


  friendlyName: 'Upload images',


  description: '',

  files: ['images'],

  inputs: {
    images: {
      type: 'ref',
      description: 'The images that the user wants to upload.',
      required: true
    }
  },


  exits: {
    noImagesAttached: {
      description: 'No images were attached.',
      responseType: 'badRequest'
    }
  },


  fn: async function (inputs, exits) {
    var url = require('url');

    var images = await sails.upload(inputs.images)
      .intercept((err) => new Error('Image upload failed.'));

    if (!images) {
      throw 'noImagesAttached'
    }

    for (var image in images) {
      var newImage = await Image.create({
        imageFd: images[image].fd,
        imageMime: images[image].type,
        owner: this.req.session.userId
      });
    }

    return this.res.redirect('/home');
  }

};
