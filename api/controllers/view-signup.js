module.exports = {


  friendlyName: 'View signup',


  description: 'Display "Signup" page.',

  inputs: {
    'wallet': {
      type: 'string'
    }
  },


  exits: {

    success: {
      viewTemplatePath: 'pages/signup'
    }

  },


  fn: async function (inputs, exits) {

    return exits.success();


  }


};
