module.exports = {


  friendlyName: 'View upload',


  description: 'Display "Upload" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/upload'
    }

  },


  fn: async function (inputs, exits) {

    // Respond with view.
    return exits.success();

  }


};
