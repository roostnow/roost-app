module.exports = function unauthorised() {

  var req = this.req;
  var res = this.res;

  sails.log.verbose('Ran custom response: res.unauthorised()');

  return res.redirect('/');

};
