/**
 * is-registered
 *
 * A simple policy that allows any request from a registered user.
 *
 * For more about how to use policies, see:
 *   https://sailsjs.com/config/policies
 *   https://sailsjs.com/docs/concepts/policies
 *   https://sailsjs.com/docs/concepts/policies/access-control-and-permissions
 */
module.exports = async function (req, res, proceed) {

  var user = await User.findOne({
    walletId: req.session.walletId
  });

  if (!user) {
    req.session.userId = undefined;
    return res.redirect('/signup');
  } else {
    req.session.userId = user.id;
    return proceed();
  }

  //--•
  // Otherwise, this request did not come from a logged-in user.

};
